<?php

namespace Infab\Shop;

use Illuminate\Database\Eloquent\Model;

class OrderRow extends Model
{
    protected $casts = [
        'options' => 'array',
        'price' => 'integer',
        'total' => 'integer',
        'qty' => 'integer',
        'order_id' => 'integer',
        'product_id' => 'integer'
    ];

    public function product()
    {
        return $this->belongsTo(\Infab\Shop\Product::class);
    }
}