<?php

namespace Infab\Shop\Repositories;

use Carbon\Carbon;
use Infab\Shop\Order;
use Infab\Shop\OrderRow;
use Infab\Shop\Events\OrderCreated;
use Infab\Shop\Events\OrderConfirmed;
use Infab\Shop\Events\OrderCancelled;
use Infab\Shop\Events\OrderProcessed;
use Illuminate\Contracts\Events\Dispatcher;
use Infab\Shop\Repositories\Contracts\OrderRepositoryInterface;

class OrderRepository implements OrderRepositoryInterface
{
    /**
     * The event dispatcher instance.
     *
     * @var \Illuminate\Contracts\Events\Dispatcher
     */
    protected $events;

    /**
     * @var object Infab\Shop\Order
     */
    private $model;

    /**
     * Constructor
     *
     * @param Infab\Shop\Order $model
     * @param Illuminate\Contracts\Events\Dispatcher $events
     */
    public function __construct(Order $model, Dispatcher $events)
    {
        $this->model = $model;
        $this->events = $events; 
    }

    public function all($eagerLoad, $number, bool $execute = false)
    {
        $paginator = $this->model->with($eagerLoad);

        if ($execute) {
            return $paginator->paginate($number);
        }
        return $paginator;
    }

    public function find($eagerLoad, $id)
    {
        return $this->model->with($eagerLoad)->where('id', $id)->first();
    }

    public function create($eagerLoad, array $data)
    {
        $this->model->user_id = $data['customer']['id'];
        $this->model->custom_properties = (isset($data['custom_properties'])) ? (array) $data['custom_properties'] : [];
        $this->model->save();
        $rows = $this->createOrderRows($data['cart']);

        $this->events->fire(new OrderCreated($this->model));

        return $this->model->load($eagerLoad);
    }

    public function cancel($eagerLoad, $id)
    {
        $model = $this->model->with($eagerLoad)->where('id', $id)->first();
        if(! $model) {
            return null;
        }
        $model->cancelled_at = Carbon::now();
        $model->save();
        $this->events->fire(new OrderCancelled($model));

        return $model;
    }

    /**
     * Sets the order to confirmed
     *
     * @param array $eagerLoad
     * @param string|integer $id
     * @return object Infab\Shop\Order
     */
    public function confirm($eagerLoad, $id)
    {
        $model = $this->model->with($eagerLoad)->where('id', $id)->first();
        if(! $model) {
            return null;
        }
        $model->confirmed_at = Carbon::now();
        $model->save();
        $this->events->fire(new OrderConfirmed($model));

        return $model;
    }

    /**
     * Sets the order to processed
     *
     * @param array $eagerLoad
     * @param string|integer $id
     * @return object Infab\Shop\Order
     */
    public function process($eagerLoad, $id)
    {
        $model = $this->model->with($eagerLoad)->where('id', $id)->first();
        if(! $model) {
            return null;
        }
        $model->processed_at = Carbon::now();
        $model->save();
        $this->events->fire(new OrderProcessed($model));

        return $model;
    }

    protected function createOrderRows(array $cartArray)
    {
        $rows = collect($cartArray)->each(function ($cartRow) {
            $row = new OrderRow;
            $row->order_id = $this->model->id;
            $row->product_id = $cartRow['id'];
            $row->qty = $cartRow['qty'];
            $row->price = $cartRow['price'];
            $row->total = $cartRow['price'] * $cartRow['qty'];
            $row->name = $cartRow['name'];
            $row->options = $cartRow['options'];
            $row->sku = $cartRow['options']['sku'];
            $row->save();
        });

        return $rows;
    }
}
