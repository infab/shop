<?php
namespace Infab\Shop\Repositories\Contracts;

interface OrderRepositoryInterface
{
    public function all($eagerLoad, $number, bool $execute);

    public function find($eagerLoad, $id);

    public function create($eagerLoad, array $data);

    public function cancel($eagerLoad, $id);

    public function process($eagerLoad, $id);

    public function confirm($eagerLoad, $id);
}
