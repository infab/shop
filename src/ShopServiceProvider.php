<?php

namespace Infab\Shop;

use Infab\Shop\Order;
use Illuminate\Contracts\Events\Dispatcher;
use Infab\Shop\Repositories\OrderRepository;
use Infab\Shop\Repositories\Contracts\OrderRepositoryInterface;
use Illuminate\Support\ServiceProvider;

class ShopServiceProvider extends ServiceProvider
{
    /**
     * Boot the service provider.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            __DIR__.'/config/infabshop.php' => config_path('infabshop.php'),
        ]);

        $this->publishes([
            __DIR__.'/database/migrations/create_products_table.php' => database_path('migrations/'.date('Y_m_d_His', time()).'_create_products_table.php')
        ]);
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(__DIR__.'/config/infabshop.php', 'infabshop');

        $this->app->singleton(OrderRepositoryInterface::class, function () {
            return  $repo = new OrderRepository(new Order,  app(Dispatcher::class));
        });
    }
}
