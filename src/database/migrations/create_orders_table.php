<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamp('cancelled_at')->nullable();
            $table->timestamp('confirmed_at')->nullable();
            $table->timestamp('processed_at')->nullable();
            $table->longText('custom_properties')->nullable();
            $table->integer('user_id')->unsigned()->nullable();
            $table->timestamps();
        });

        Schema::create('order_rows', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('order_id')->unsigned()->index();
            $table->foreign('order_id')->references('id')->on('orders')->onDelete('cascade');
            $table->integer('product_id');
            $table->integer('total');
            $table->integer('price');
            $table->integer('qty');
            $table->string('sku');
            $table->string('name');
            $table->text('options')->nullable();
            $table->boolean('processed')->default(0);
            $table->timestamps();
        });
       
    }
    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('orders');
        Schema::dropIfExists('order_rows');
    }
}