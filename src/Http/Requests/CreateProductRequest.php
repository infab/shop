<?php

namespace Infab\Shop\Http\Requests;

use Illuminate\Foundation\Http\FormRequest as Request;

class CreateProductRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'sku' => 'required|unique:products',
        ];
    }

    public function messages() {
        return [
            'name.required' => 'Ange ett namn',
            'sku.required' => 'Ange ett artikelnummer',
            'sku.unique' => 'Artikelnumret är upptaget',
        ];
    }
}
