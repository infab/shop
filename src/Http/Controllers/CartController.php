<?php

namespace Infab\Shop\Http\Controllers;

use Infab\Shop\Product;
use Illuminate\Http\Request;
use Gloudemans\Shoppingcart\Cart;
use Infab\Shop\Transformers\ProductTransformer;
use Infab\Core\Http\Controllers\Api\ApiController;

class CartController extends ApiController 
{

    public function index(Request $request, Cart $cart)
    {
        return $this->respondWithArray([
            'data' => [
                'cart' => $cart->content(),
                'cart_count' => $cart->count(),
            ]
        ]);
    }

    public function store(Request $request, Cart $cart, $id)
    {
        $eagerLoad = $this->eagerLoad($request);
        $product = Product::where('id', $id)->with($eagerLoad)->first();
        if (! $product) {
            return $this->errorNotFound('Product not found');
        }
        $cart->add($product, $request->quantity, $request->options);

        return $this->respondWithArray([
            'data' => [
                'cart' => $cart->content(),
                'cart_count' => $cart->count(),
            ]
        ]);
    }

    public function removeRow(Request $request, Cart $cart)
    {
        if(! $request->has('row_id')) {
            return $this->errorWrongArgs('Cart row not found');
        }
        $cart->remove($request->row_id);

        return $this->respondWithArray([
            'data' => [
                'cart' => $cart->content(),
                'cart_count' => $cart->count(),
            ]
        ]);
    }

    public function updateRow(Request $request, Cart $cart)
    {
        if(! $request->has('row_id')) {
            return $this->errorWrongArgs('Cart row not found');
        }
        
        $cart->update($request->row_id, $request->values);

        return $this->respondWithArray([
            'data' => [
                'cart' => $cart->content(),
                'cart_count' => $cart->count(),
            ]
        ]);
    }

    public function destroy(Cart $cart)
    {
        $cart->destroy();

        return $this->respondWithArray([
            'data' => [
                'cart' => $cart->content(),
                'cart_count' => $cart->count(),
            ]
        ]);
    }
    
}