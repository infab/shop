<?php

namespace Infab\Shop\Http\Controllers;

use Illuminate\Http\Request;
use Infab\Shop\Product;
use Infab\Shop\Transformers\ProductTransformer;
use Infab\Shop\Http\Requests\CreateProductRequest;
use Infab\Shop\Http\Requests\UpdateProductRequest;
use Infab\Core\Http\Controllers\Api\ApiController;

class ProductsController extends ApiController
{

    public function index(Request $request)
    {
        $eagerLoad = $this->eagerLoad($request);
        $orderBy = $request->get('orderBy', 'sku'); 
        $direction = $request->get('direction', 'desc');
        $paginator = Product::with($eagerLoad);
        if($request->has('search')) {
            $paginator->search($request->search);
        }
        $paginator = $paginator->orderBy($orderBy, $direction)->paginate($this->number);

        return $this->respondWithPaginator($paginator, new ProductTransformer);
    }

    public function show(Request $request, $id)
    {
        $eagerLoad = $this->eagerLoad($request);
        $product = Product::where('id', $id)->with($eagerLoad)->first();
        if (! $product) {
            return $this->errorNotFound('Product not found');
        }

        return $this->respondWithItem($product, new ProductTransformer);
    }

    public function store(CreateProductRequest $request)
    {
        $product = new Product;
        $product->fill($request->all());
        $product->save();

        return $this->respondWithItem($product, new ProductTransformer, 201);
    }

    public function update(UpdateProductRequest $request, $id)
    {
        $product = Product::find($id);
        if (! $product) {
            return $this->errorNotFound('Product not found');
        }
        $product->fill($request->all());
        $product->save();

        return $this->respondWithItem($product, new ProductTransformer, 200);
    }

    public function destroy($id)
    {
        $product = Product::find($id);
        if (! $product) {
            return $this->errorNotFound('Product not found');
        }
        $product->delete();

        return $this->respondWithSuccess('Product has been deleted');
    }

    public function search(Request $request)
    {
        if(! $request->has('q')) {
            return $this->errorWrongArgs('Query is not specified');
        }
        $eagerLoad = $this->eagerLoad($request);
        $products = Product::search($request->q)->with($eagerLoad)->paginate($this->number);

        return $this->respondWithPaginator($products, new ProductTransformer);
    }
}
