<?php

namespace Infab\Shop\Http\Controllers;

use Illuminate\Http\Request;
use Infab\Shop\Order;
use Infab\Shop\Transformers\OrderTransformer;
use Infab\Core\Http\Controllers\Api\ApiController;
use Infab\Shop\Repositories\Contracts\OrderRepositoryInterface as OrderRepository;

class OrdersController extends ApiController
{
    /**
     * Defines which relationships that can be eager loaded
     * @var array
     */
    public $possibleRelationships = [
        'rows'   => 'rows',
        'rows.product' => 'rows.product'
    ];

    
    public function index(Request $request)
    {
        // TODO; clean this up
        $eagerLoad = $this->eagerLoad($request);
        $orderBy = $request->get('orderBy', 'created_at'); 
        $direction = $request->get('direction', 'desc');
        $paginator = Order::with($eagerLoad);
        if($request->has('search')) {
            $paginator->search($request->search);
        }
        $paginator = $paginator->orderBy($orderBy, $direction)->paginate($this->number);

        return $this->respondWithPaginator($paginator, new OrderTransformer);
    }

    public function show(Request $request, OrderRepository $repo, $id)
    {
        $eagerLoad = $this->eagerLoad($request);
        $order = $repo->find($eagerLoad, $id);
        if (! $order) {
            return $this->errorNotFound('Order not found');
        }

        return $this->respondWithItem($order, new OrderTransformer);
    }

    public function cancel(Request $request, OrderRepository $repo, $id)
    {
        $eagerLoad = $this->eagerLoad($request);
        $order = $repo->cancel($eagerLoad, $id);
        if (! $order) {
            return $this->errorNotFound('Order not found');
        } 

        return $this->respondWithItem($order, new OrderTransformer);
    }

    public function confirm(Request $request, OrderRepository $repo, $id)
    {
        $eagerLoad = $this->eagerLoad($request);
        $order = $repo->confirm($eagerLoad, $id);
        if (! $order) {
            return $this->errorNotFound('Order not found');
        }

        return $this->respondWithItem($order, new OrderTransformer);
    }

    public function process(Request $request, OrderRepository $repo, $id)
    {
        $eagerLoad = $this->eagerLoad($request);
        $order = $repo->process($eagerLoad, $id);
        if (! $order) {
            return $this->errorNotFound('Order not found');
        }

        return $this->respondWithItem($order, new OrderTransformer);
    }

    public function store(Request $request, OrderRepository $repo)
    {
        $eagerLoad = $this->eagerLoad($request);
        $order = $repo->create($eagerLoad, $request->all());

        return $this->respondWithItem($order, new OrderTransformer, 201);
    }

    public function destroy(Request $request, $id)
    {
        $order = Order::where('id', $id)->first();
        if (! $order) {
            return $this->errorNotFound('Order not found');
        }

        $order->delete();
        $this->respondWithSuccess('Order has been deleted');
    }
}
