<?php

namespace Infab\Shop\Events;

class OrderProcessed
{
    /**
     * The newly created refresh token ID.
     *
     * @var string
     */
    public $order;

    /**
     * Create a new event instance.
     *
     * @param  integer  $orderId
     * @return void
     */
    public function __construct($order)
    {
        $this->order = $order;
    }
}
