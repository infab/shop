<?php

namespace Infab\Shop;

use Illuminate\Database\Eloquent\Model;
use Nicolaslopezj\Searchable\SearchableTrait;

class Order extends Model
{
    use SearchableTrait;

    protected $casts = [
        'custom_properties' => 'array'
    ];

    /**
     * Searchable rules.
     *
     * Columns and their priority in search results.
     * Columns with higher values are more important.
     * Columns with equal values have equal importance.
     * 
     * @var array
     */
    protected $searchable = [
        'columns' => [
            'orders.id' => 10,
            'orders.custom_properties' => 20,
            'order_rows.name' => 7,
            'order_rows.sku' => 7,
            'order_rows.options' => 2,
        ],
        'joins' => [
            'order_rows' => ['orders.id','order_rows.order_id'],
        ],
        'groupBy' => 'orders.id' // defaults to false
    ];

    public function rows()
    {
        return $this->hasMany(\Infab\Shop\OrderRow::class);
    }
}