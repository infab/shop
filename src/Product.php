<?php

namespace Infab\Shop;

use Illuminate\Database\Eloquent\Model;
use Nicolaslopezj\Searchable\SearchableTrait;
use Gloudemans\Shoppingcart\Contracts\Buyable;

class Product extends Model implements Buyable
{
    use SearchableTrait;

    protected $fillable = ['name', 'sku', 'specification', 'description', 'custom_properties', 'price', 'published'];

    protected $casts = [
        'custom_properties' => 'array'
    ];

    /**
     * Searchable rules.
     *
     * Columns and their priority in search results.
     * Columns with higher values are more important.
     * Columns with equal values have equal importance.
     * 
     * @var array
     */
    protected $searchable = [
        'columns' => [
            'products.sku' => 10,
            'products.name' => 10,
            'products.id' => 5,
            'products.description' => 2,
            'products.specification' => 2,
            'products.custom_properties' => 1,
        ],
        'groupBy' => 'products.id'
    ];

     /**
     * Get the identifier of the Buyable item.
     *
     * @return int|string
     */
    public function getBuyableIdentifier($options = null)
    {
        return $this->id;
    }

    /**
     * Get the description or title of the Buyable item.
     *
     * @return string
     */
    public function getBuyableDescription($options = null)
    {
        return $this->name;
    }

    /**
     * Get the price of the Buyable item.
     *
     * @return float
     */
    public function getBuyablePrice($options = null)
    {
        return $this->price;
    }
}