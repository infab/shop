<?php

namespace Infab\Shop\Transformers;

use Infab\Shop\OrderRow;
use League\Fractal\TransformerAbstract;
use Infab\Shop\Transformers\ProductTransformer;

class OrderRowTransformer extends TransformerAbstract
{
    protected $availableIncludes = ['product'];

    public function transform(OrderRow $row)
    {
        return [
            "id"         => (int) $row->id,
            "order_id"   => (int) $row->order_id,
            "product_id" => (int) $row->product_id,
            "price"      => (int) $row->price,
            "total"      => (int) $row->price,
            "qty"        => (int) $row->qty,
            "sku"        => (string) $row->sku,
            "name"       => (string) $row->name,
            "options"    => (array) $row->options,
            "processed"  => (boolean) $row->processed,
            "created_at" => (string) $row->created_at,
            "updated_at" => (string) $row->updated_at,
        ];
    }

    public function includeProduct(OrderRow $row)
    {
        $product = $row->product;
        if(! $product) {
            return $this->null();
        }
        return $this->item($product, new ProductTransformer);
    }
}