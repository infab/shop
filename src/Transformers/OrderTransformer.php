<?php

namespace Infab\Shop\Transformers;

use Infab\Shop\Order;
use League\Fractal\TransformerAbstract;
use Infab\Shop\Transformers\OrderRowTransformer;

class OrderTransformer extends TransformerAbstract
{
    protected $availableIncludes = [
        'rows',
    ];

    public function transform(Order $order)
    {
        return [
            'id'                => (int) $order->id,
            'cancelled_at'      => (string) $order->cancelled_at,
            'confirmed_at'      => (string) $order->confirmed_at,
            'processed_at'      => (string) $order->processed_at,
            'custom_properties' => (array) $order->custom_properties,
            'user_id'           => (int) $order->user_id,
            'updated_at'        => (string) $order->updated_at,
            'created_at'        => (string) $order->created_at,
        ];
    }

    public function includeRows(Order $order)
    {
        $rows = $order->rows;

        return $this->collection($rows, new OrderRowTransformer);
    }
}