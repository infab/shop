<?php

namespace Infab\Shop\Transformers;

use Infab\Shop\Product;
use League\Fractal\TransformerAbstract;

class ProductTransformer extends TransformerAbstract
{
    public function transform(Product $product)
    {
        return [
            'id'                => (int) $product->id,
            'sku'               => (string) $product->sku,
            'name'              => (string) $product->name,
            'description'       => (string) $product->description,
            'specification'     => (string) $product->specification,
            'custom_properties' => (array) $product->custom_properties,
            'price'             => (integer) $product->price,
            'published'         => (bool) $product->published,
            'updated_at'        => (string) $product->updated_at
        ];
    }
}