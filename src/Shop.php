<?php

namespace Infab\Shop;

use Infab\Shop\RouteRegistrar;
use Illuminate\Support\Facades\Route;

class Shop
{
    /**
     * Binds the Shop routes into the controller.
     *
     * @param  callable|null  $callback
     * @param  array  $options
     * @return void
     */
    public static function routes($callback = null, array $options = [])
    {
        $callback = $callback ?: function ($router) {
            $router->all();
        };

        $defaultOptions = [
            'prefix' => '',
            'namespace' => '\Infab\Shop\Http\Controllers',
        ];

        $options = array_merge($defaultOptions, $options);

        Route::group($options, function ($router) use ($callback) {
            $callback(new RouteRegistrar($router));
        });
    }
}
