<?php

namespace Infab\Shop;

use Illuminate\Contracts\Routing\Registrar as Router;

class RouteRegistrar
{
    /**
     * The router implementation.
     *
     * @var \Illuminate\Contracts\Routing\Registrar
     */
    protected $router;
    
    /**
     * Create a new route registrar instance.
     *
     * @param  \Illuminate\Contracts\Routing\Registrar  $router
     * @return void
     */
    public function __construct(Router $router)
    {
        $this->router = $router;
    }

    public function all()
    {
        $this->forProducts();
        $this->forCart();
        $this->forOrders();
    }

    public function forProducts()
    {
        $this->router->group(['middleware' => ['api']], function ($router) {
            $router->get('/products', 'ProductsController@index');
            $router->post('/products', 'ProductsController@store');
            $router->get('/products/search', 'ProductsController@search');
            $router->get('/products/{id}', 'ProductsController@show');
            $router->patch('/products/{id}', 'ProductsController@update');
            $router->delete('/products/{id}', 'ProductsController@destroy');
        });
    }

    public function forCart()
    {
        $this->router->get('cart', 'CartController@index');
        $this->router->delete('cart', 'CartController@destroy');
        $this->router->post('cart/add/{id}', 'CartController@store');
        $this->router->delete('cart/remove', 'CartController@removeRow');
        $this->router->patch('cart/update', 'CartController@updateRow');
    }

    public function forOrders()
    {
        $this->router->group(['middleware' => ['api']], function ($router) {
            $router->get('/orders', 'OrdersController@index');
            $router->post('/orders', 'OrdersController@store');
            $router->get('/orders/{id}', 'OrdersController@show');
            $router->delete('/orders/{id}', 'OrdersController@destroy');
            $router->post('/orders/{id}/cancel', 'OrdersController@cancel');
            $router->post('/orders/{id}/confirm', 'OrdersController@confirm');
            $router->post('/orders/{id}/process', 'OrdersController@process');
        });
    }
}
