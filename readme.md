# infab/shop

*Adds shop support to IQ CMS*

[![pipeline status](https://gitlab.com/infab/shop/badges/master/pipeline.svg)](https://gitlab.com/infab/shop/commits/master)
[![coverage report](https://gitlab.com/infab/shop/badges/master/coverage.svg)](https://gitlab.com/infab/shop/commits/master)


Publish the config

```bash
php artisan vendor:publish --provider="Infab\Shop\ShopServiceProvider"
```

Register the routes for the Shop package within the `boot` method of your `AuthServiceProvider`

```php
<?php
namespace App\Providers;

use Infab\Shop\Shop;
use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
   
    /**
     * Define your route model bindings, pattern filters, etc.
     */
    public function boot()
    {
        parent::boot();

        // Register all shop routes
        // Note that routes are public by default!
        Shop::routes();

        // Or
        
        // Only admin may use product routes
        Route::group(['middleware' => ['auth', 'auth.admin']], function() {
            Shop::routes(function ($registrar) {
                $registrar->forProducts();
            });
            Shop::routes(function ($registrar) {
                $registrar->forOrders();
            });
        });

        // Public routes
        Shop::routes(function ($registrar) {
            $registrar->forCart();
        });
    }

```