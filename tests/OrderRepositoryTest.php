<?php

namespace Infab\Shop\Test;

use Mockery as m;
use Infab\Shop\Order;
use Infab\Shop\OrderRow;
use Infab\Shop\Test\TestCase;
use Infab\Shop\Repositories\Contracts\OrderRepositoryInterface as OrderRepository1;
use Infab\Shop\Repositories\OrderRepository;
use Illuminate\Contracts\Events\Dispatcher;

class OrderRepositoryTest extends TestCase
{
    /** @test **/
    public function it_can_fetch_all_orders()
    {
        // Arrange
        $repository = new OrderRepository(new Order(), app(\Illuminate\Contracts\Events\Dispatcher::class));
       
        // Act
        $items = $repository->all('', 500, true);
        
        // Assert
        $this->assertCount(0, $items);
    }

    /** @test **/
    public function it_can_get_all_and_return_paginator_object()
    {
        // Arrange
        $repository = new OrderRepository(new Order(), app(\Illuminate\Contracts\Events\Dispatcher::class));
       
        // Act
        $items = $repository->all('', 500, false);
        
        // Assert
        $this->assertInstanceOf(\Illuminate\Database\Eloquent\Builder::class, $items);
    }

    /** @test **/
    public function it_can_get_a_single_order()
    {
        // Arrange
        $order = Order::create();
        $repository = new OrderRepository($order, app(\Illuminate\Contracts\Events\Dispatcher::class));

        // Act
        $item = $repository->find([], 1);
        
        // Assert
        $this->assertInstanceOf(Order::class, $item);
    }


    /** @test **/
    public function it_can_create_a_new_order()
    {
        // Arrange
        $repository = new OrderRepository(new Order(), app(\Illuminate\Contracts\Events\Dispatcher::class));

        // Act
        $item = $repository->create(['rows'], [
            'customer' => [
                'id' => 193
            ],
            'cart' => [
                '219380XA' => [
                    'id' => 1,
                    'qty' => 2,
                    'name' => 'Orchestra',
                    'price' => 800.0,
                    'options' => [
                        "size" => 44,
                        "color" => "red",
                        "sku" => "AR2938"
                    ]
                ],
                '12938RA' => [
                    'id' => 2,
                    'qty' => 1,
                    'name' => 'Red dead redemption',
                    'price' => 100.0,
                    'options' => [
                        "size" => 44,
                        "color" => "red",
                        "sku" => "AR293899"
                    ]
                ]
            ]
        ]);
        
        // Assert
        $this->assertEquals(1, $item->id);
        $this->assertEquals(193, $item->user_id);
        $this->assertInstanceOf(Order::class, $item);
        $this->assertInstanceOf(OrderRow::class, $item->rows->first());
    }
}