<?php

namespace Infab\Shop\Test;

use Infab\Shop\Shop;
use Illuminate\Database\Schema\Blueprint;
use Orchestra\Testbench\TestCase as Orchestra;

abstract class TestCase extends Orchestra
{
    public function setUp()
    {
        parent::setUp();

        Shop::routes();
        // Pass::routes(function ($registrar) { $registrar->forAuthorization(); });
        $this->loadLaravelMigrations(['--database' => 'testing']);
        $this->setUpDatabase($this->app);
    }


    public function setUpDatabase($app)
    {
        include_once realpath(__DIR__.'/../src/database/migrations/2018_01_15_182750_create_products_table.php');
        (new \CreateProductsTable())->up();
        include_once realpath(__DIR__.'/../src/database/migrations/2018_02_01_144412_create_orders_table.php');
        (new \CreateOrdersTable())->up();
    }
    /**
     * @param \Illuminate\Foundation\Application $app
     *
     * @return array
     */
    protected function getPackageProviders($app)
    {
        return [
            \Infab\Shop\ShopServiceProvider::class,
        ];
    }

    /**
     * @param \Illuminate\Foundation\Application $app
     */
    protected function getEnvironmentSetUp($app)
    {
        $app['config']->set('database.default', 'sqlite');
        $app['config']->set('database.connections.sqlite', [
            'driver' => 'sqlite',
            'database' => ':memory:',
            'prefix' => '',
        ]);
    }
}
