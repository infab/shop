<?php

namespace Infab\Shop\Test;

use Infab\Shop\Test\TestCase;

// use Illuminate\Foundation\Testing\DatabaseMigrations;

class ProductsControllerTest extends TestCase
{
    public function setUp()
    {
        parent::setUp();
        $this->withoutMiddleware();
    }

    /** @test **/
    public function it_can_get_all_products()
    {
        \DB::table('products')->insert([
            'name' => 'Orchestra',
            'sku' => 'fop',
        ]);
        \DB::table('products')->insert([
            'name' => 'New Product',
            'sku' => 'new213',
        ]);
        $this->withoutExceptionHandling();

        $response = $this->json('GET', '/products');

        $response->assertStatus(200);
        $this->assertEquals(2, count(json_decode($response->content())->data));
    }

    /** @test **/
    public function it_can_get_a_single_product()
    {
        //Arrange
        \DB::table('products')->insert([
            'name' => 'New Product',
            'sku' => 'new213',
        ]);
        $this->withoutExceptionHandling();

        // Act
        $response = $this->json('GET', '/products/1');
    
        //Assert
        $response->assertStatus(200);
        $this->assertEquals("new213", json_decode($response->content())->data->sku);

        // We expect there to be 9 properties
        $this->assertEquals(9, count(json_decode($response->content(), true)['data']));

        // We expect the types to be correct
        $product = json_decode($response->content())->data;
        $this->assertInternalType('integer', $product->id);
        $this->assertInternalType('string', $product->sku);
        $this->assertInternalType('string', $product->name);
        $this->assertInternalType('string', $product->description);
        $this->assertInternalType('string', $product->specification);
        $this->assertInternalType('array', $product->custom_properties);
        $this->assertInternalType('boolean', $product->published);
        $this->assertInternalType('string', $product->updated_at);
    }

    /** @test **/
    public function it_will_return_404_if_no_product_is_found()
    {
        // Arrange
        $this->withoutExceptionHandling();
    
        // Act
        $response = $this->json('GET', '/products/1');
    
        // Assert
        $response->assertStatus(404);
        $response->assertJson([
            'error' => [
                'message' => 'Product not found'
            ]
        ]);
    }

    /** @test **/
    public function a_product_can_be_created()
    {
        // Arrange
        $this->withoutExceptionHandling();
    
        // Act
        $response = $this->json('POST', '/products', [
            'name' => 'Prop product',
            'sku' => 'AXSA01239'
        ]);
    
        // Assert
        $response->assertStatus(201);
        $this->assertDatabaseHas('products', [
            'name' => 'Prop product',
            'sku' => 'AXSA01239'
        ]);
        $response->assertJson([
            'data' => [
                'name' => 'Prop product',
                'sku' => 'AXSA01239'
            ]
        ]);
    }

    /** @test **/
    public function input_is_validated_before_storing_a_new_product()
    {
        // Act
        $response = $this->json('POST', '/products', []);
    
        // Assert
        $response->assertStatus(422);
        $response->assertJson([
            'errors' => [
                'name' => ['Ange ett namn'],
                'sku' => ['Ange ett artikelnummer']
            ]
        ]);
    }

    /** @test **/
    public function a_product_can_be_updated()
    {
        // Arrange
        \DB::table('products')->insert([
            'id' => 1,
            'name' => 'Orchestra',
            'sku' => 'AR3189',
        ]);
    
        // Act
        $response = $this->json('patch', '/products/1', [
            'id' => 1,
            'name' => 'Orca 2',
            'sku' => 'AR3189',
            'description' => 'Lorem ipsum',
            'specification' => 'The specs',
            'custom_properties' => ['key' => 'value'],
            'price' => 10000,
            'published' => true
        ]);
    
        // Assert
        $response->assertStatus(200);
        $this->assertDatabaseHas('products', [
            'name' => 'Orca 2',
            'sku' => 'AR3189',
            'description' => 'Lorem ipsum',
            'specification' => 'The specs',
            'custom_properties' => "{\"key\":\"value\"}",
            'price' => 10000,
            'published' => 1
        ]);
        $response->assertJson([
            'data' => [
                'name' => 'Orca 2',
                'sku' => 'AR3189',
                'description' => 'Lorem ipsum',
                'specification' => 'The specs',
                'price' => 10000,
                'custom_properties' => [
                    'key' => 'value'
                ],
                'published' => true
            ]
        ]);
    }

    /** @test **/
    public function it_will_return_404_if_no_prod_is_found_when_updating()
    {
        // Act
        $response = $this->json('patch', '/products/1', [
            'name' => 'Orca 2',
            'sku' => 'Orca 2',
        ]); 
    
        // Assert
        $response->assertStatus(404);
    }

    /** @test **/
    public function it_will_validate_input_when_updating_a_product()
    {
        // Arrange
        \DB::table('products')->insert([
            'id' => 1,
            'name' => 'Orchestra',
            'sku' => 'P1',
        ]);
    
        \DB::table('products')->insert([
            'id' => 2,
            'name' => 'Orchestra',
            'sku' => 'P2',
        ]);

        // Act
        $response = $this->json('patch', 'products/1', [
            'name' => 'Changed',
            'sku' => 'P2'
        ]);
    
        // Assert
        $response->assertStatus(422);
    }

    /** @test **/
    public function it_can_delete_a_product()
    {
        // Arrange
        $this->withoutExceptionHandling();
        \DB::table('products')->insert([
            'name' => 'Orchestra',
            'sku' => 'fop',
        ]);
    
        // Act
        $response = $this->json("DELETE", '/products/1');
    
        // Assert
        $response->assertStatus(200);
        $this->assertDatabaseMissing('products', [
            'id' => 1,
            'name' => 'Orchestra',
            'sku' => 'fop',
        ]);
    }

    /** @test **/
    public function it_will_return_404_if_no_product_is_found_when_deleting()
    {
        // Act
        $response = $this->json("DELETE", '/products/1');
    
        // Assert
        $response->assertStatus(404);
    }

    /** @test **/
    public function products_are_searchable()
    {
        // Arrange
        $this->withoutExceptionHandling();
        \DB::table('products')->insert([
            'name' => 'Orchestra',
            'sku' => 'fop',
        ]);

        \DB::table('products')->insert([
            'name' => 'Another product',
            'sku' => 'AR21983',
        ]);
    
        // Act
        $response = $this->json('GET', '/products/search?q=AR21');
    
        // Assert
        $response->assertStatus(200);
        $data = json_decode($response->content(), true)['data'];
        $this->assertEquals(1, count($data));
        $this->assertEquals('AR21983', $data[0]['sku']);
    }

    /** @test **/
    public function a_query_is_required_to_perform_a_search()
    {
        // Act
        $response = $this->json('GET', '/products/search');
    
        // Assert
        $response->assertStatus(400);
        $this->assertEquals('Query is not specified', json_decode($response->content(),true)['error']['message']);
    }

    /** @test **/
    public function it_can_search_from_the_index_method()
    {
        // Arrange
        $this->withoutExceptionHandling();
        \DB::table('products')->insert([
            'name' => 'Orchestra',
            'sku' => 'fop',
        ]);

        \DB::table('products')->insert([
            'name' => 'Another product',
            'sku' => 'AR21983',
        ]);
    
        // Act
        $response = $this->json('GET', '/products/?search=AR21');
    
        // Assert
        $response->assertStatus(200);
        $data = json_decode($response->content(), true)['data'];
        $this->assertEquals(1, count($data));
        $this->assertEquals('AR21983', $data[0]['sku']); 
    }
}
