<?php

namespace Infab\Shop\Test;

use Infab\Shop\Test\TestCase;
use Gloudemans\Shoppingcart\Cart;

class CartControllerTest extends TestCase
{

    /** @test **/
    public function it_can_get_the_cart_content()
    {
        // Arrange
        $product = \DB::table('products')->insert([
            'name' => 'Orchestra',
            'sku' => 'AR2938',
            'price' => 800
        ]);
        $this->json('post', '/cart/add/1', [
            'quantity' => 2,
            'options' => ['size' => 44, 'color' => 'red', 'sku' => 'AR2938']
        ]);
    
        // Act
        $response = $this->json('get', '/cart');
    
        // Assert
        $response->assertStatus(200);
        $response->assertJson([
            'data' => [
                'cart' => [

                ],
                'cart_count' => 2
            ]
        ]);
    }
    
    /** @test **/
    public function it_can_add_a_product_to_the_cart()
    {
        $product = \DB::table('products')->insert([
            'name' => 'Orchestra',
            'sku' => 'AR2938',
            'price' => 800
        ]);
        $this->withoutExceptionHandling();

        $response = $this->json('post', '/cart/add/1', [
            'quantity' => 2,
            'options' => ['size' => 44, 'color' => 'red', 'sku' => 'AR2938']
        ]);

        $response->assertStatus(200);
        $cart = json_decode($response->content(), true)['data'];
        $cartValues = array_values($cart['cart']);

        $this->assertEquals(2, $cart['cart_count']);
        $this->assertEquals(800, $cartValues[0]['price']);
        $this->assertEquals(1600, $cartValues[0]['subtotal']);
        $this->assertEquals(2, $cartValues[0]['qty']);
        $this->assertEquals(44, $cartValues[0]['options']['size']);
        $this->assertEquals('AR2938', $cartValues[0]['options']['sku']);
        $this->assertEquals('red', $cartValues[0]['options']['color']);
    }

    /** @test **/
    public function it_will_return_404_if_the_product_isnt_found()
    {
        // Act
        $response = $this->json('post', '/cart/add/1', [
            'quantity' => 2,
            'options' => ['size' => 44, 'color' => 'red', 'sku' => 'AR2938']
        ]);
    
        // Assert
        $response->assertStatus(404);
    }

    /** @test **/
    public function it_can_remove_a_row_from_the_cart()
    {
        // Arrange
        $product = \DB::table('products')->insert([
            'name' => 'Orchestra',
            'sku' => 'AR2938',
            'price' => 800
        ]);
        $this->withoutExceptionHandling();

        $response = $this->json('post', '/cart/add/1', [
            'quantity' => 2,
            'options' => ['size' => 44, 'color' => 'red', 'sku' => 'AR2938']
        ]);
        $rowId = array_keys(json_decode($response->content(), true)['data']['cart'])[0];
    
        // Act
        $response = $this->json('delete', '/cart/remove', [
            'row_id' => $rowId
        ]);
    
        // Assert
        $response->assertStatus(200);
        $cart = json_decode($response->content(), true)['data'];
        $this->assertEquals(0, $cart['cart_count']);
    }

    /** @test **/
    public function it_will_return_400_if_no_row_id_is_specified()
    {
        // Act
        $response = $this->json('delete', '/cart/remove');

        // Assert
        $response->assertStatus(400);
        $this->assertEquals('Cart row not found', json_decode($response->content(), true)['error']['message']);
    }

    /** @test **/
    public function it_can_empty_the_cart()
    {
        // Arrange
        $product = \DB::table('products')->insert([
            'name' => 'Orchestra',
            'sku' => 'AR2938',
            'price' => 800
        ]);
        $this->withoutExceptionHandling();

        $response = $this->json('post', '/cart/add/1', [
            'quantity' => 2,
            'options' => ['size' => 44, 'color' => 'red', 'sku' => 'AR2938']
        ]); 

        // Act
        $response = $this->json('delete', '/cart');

        // Assert
        $response->assertStatus(200);
        $this->assertEquals([], json_decode($response->content(),true)['data']['cart']);
    }

    /** @test **/
    public function a_row_can_be_updated()
    {
         // Arrange
         $product = \DB::table('products')->insert([
            'name' => 'Orchestra',
            'sku' => 'AR2938',
            'price' => 800
        ]);
        $this->withoutExceptionHandling();

        $response = $this->json('post', '/cart/add/1', [
            'quantity' => 2,
            'options' => ['size' => 44, 'color' => 'red', 'sku' => 'AR2938']
        ]);
        $rowId = array_keys(json_decode($response->content(), true)['data']['cart'])[0];

        // Act
        $response = $this->json('patch', '/cart/update', [
            'row_id' => $rowId,
            'values' => [
                'qty' => 40,
                'options' => ['size' => 38, 'color' => 'red', 'sku' => 'AR2938']
            ]
        ]);
    
        // Assert
        $response->assertStatus(200);
        $cart = json_decode($response->content(), true)['data'];
        $cartValues = array_values($cart['cart']);
        $this->assertEquals(40, $cart['cart_count']);
        $this->assertEquals(38, $cartValues[0]['options']['size']);
    }

    /** @test **/
    public function it_will_return_400_code_when_updating_nonexisting_row()
    {
    
        // Act
        $response = $this->json('patch', '/cart/update', [
            'values' => [
                'qty' => 40,
                'options' => ['size' => 38, 'color' => 'red', 'sku' => 'AR2938']
            ]
        ]);
    
        // Assert
        $response->assertStatus(400);
    }
}
