<?php

namespace Infab\Shop\Test;

use Infab\Shop\Order;
use Infab\Shop\Test\TestCase;
use Infab\Shop\Events\OrderCreated;
use Infab\Shop\Events\OrderConfirmed;
use Infab\Shop\Events\OrderCancelled;
use Infab\Shop\Events\OrderProcessed;
use Illuminate\Support\Facades\Event;
use Infab\Shop\Repositories\OrderRepository;

class OrdersControllerTest extends TestCase
{
    public function setUp()
    {
        parent::setUp();
        $this->withoutMiddleware();
    }

    /** @test **/
    public function it_can_get_all_orders()
    {
        // Arrange
        $this->withoutExceptionHandling();
        $order = $this->createOrderWithRows();

        // Act
        $response = $this->json('GET', '/orders');

        // Assert
        $response->assertStatus(200);
        $this->assertCount(1, json_decode($response->content())->data);
    }

    /** @test **/
    public function it_can_search_for_orders_using_query_param()
    {
        // Arrange
        $this->withoutExceptionHandling();
        $order = $this->createOrderWithRows('Orchestra');
        $order = $this->createOrderWithRows('Warpath');
    
        // Act
        $response = $this->json('GET', '/orders?search=Orchestra');
    
        // Assert
        $response->assertStatus(200);
        $data = json_decode($response->content(), true)['data'];
        $this->assertCount(1, $data);
    }

    /** @test **/
    public function it_can_include_its_order_rows()
    {
        // Arrange
        $this->withoutExceptionHandling();
        $order = $this->createOrderWithRows();
        
    
        // Act
        $response = $this->json('GET', '/orders/?include=rows');
    
        // Assert
        $response->assertStatus(200);
        $data = json_decode($response->content(), true)['data'];
        // Expect two rows
        $this->assertCount(2, $data[0]['rows']['data']);
        $this->assertEquals($data[0]['rows']['data'][0]['sku'], 'AR2938');
    }

    /** @test **/
    public function it_can_get_a_single_order()
    {
        // Arrange
        $this->withoutExceptionHandling();
        $order =  $this->createOrderWithRows();
    
        // Act
        $response = $this->json('GET', '/orders/' . $order->id);
    
        // Assert
        $response->assertStatus(200);
        $response->assertJson([
            'data' => [
                'id' => $order->id,
                'cancelled_at' => '',
                'user_id' => 193,
                'processed_at' => '',
                'confirmed_at' => ''
            ]
        ]);
    }


    /** @test **/
    public function it_cant_destroy_if_not_found()
    {
    
        // Act
        $response = $this->json('DELETE', '/orders/821');
    
        // Assert
        $response->assertStatus(404);
    }

    /** @test **/
    public function it_will_return_404_if_no_order_is_found()
    {
        // Act
        $response = $this->json('GET', '/orders/31');
    
        // Assert
        $response->assertStatus(404);
        $response->assertJson([
            'error' => [
                'message' => 'Order not found'
            ]
        ]);
    }

    /** @test **/
    public function it_can_set_an_order_to_confirmed()
    {
        // Arrange
        Event::fake();
        $this->withoutExceptionHandling();
        $order = $this->createOrderWithRows();

        // Act
        $response = $this->json('POST', '/orders/1/confirm');
    
        // Assert
        $response->assertStatus(200);
        $this->assertDatabaseMissing('orders', [
            'id' => 1,
            'confirmed_at' => ''
        ]);
        Event::assertDispatched(OrderConfirmed::class, function ($e) use ($order) {
            return $e->order->id === $order->id;
        });

    }

    /** @test **/
    public function it_will_return_when_confirming_if_not_found()
    {
        $this->withoutExceptionHandling();

        // Act
        $response = $this->json('POST', '/orders/1/confirm');
    
        // Assert
        $response->assertStatus(404);
    }

    /** @test **/
    public function it_can_include_order_rows_with_actual_products()
    {
        // Arrange
        $this->withoutExceptionHandling();
        $order = $this->createOrderWithRowsWithRealProducts();
    
        // Act
        $response = $this->json('GET', '/orders/1?include=rows,rows.product');
    
        // Assert
        $response->assertStatus(200);
        $response->assertJson([
            'data' => [
                'id' => $order->id,
                'rows' => [
                    'data' => [
                        0 => [
                            'name' => 'El chapo',
                            'product' => [
                                'data' => [
                                    'sku' => 'AE9301',
                                    'price' => 1400
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        ]);
    }

    /** @test **/
    public function it_will_return_null_object_if_no_relation_exist_for_product()
    {
        // Arrange
        $this->withoutExceptionHandling();
        $order = $this->createOrderWithRows();
    
        // Act
        $response = $this->json('GET', '/orders/1?include=rows,rows.product');
    
        // Assert
        $response->assertStatus(200);
        $response->assertJson([
            'data' => [
                'id' => $order->id,
                'rows' => [
                    'data' => [
                        0 => [
                            'name' => 'Orchestra',
                            'product' => [
                                'data' => []
                            ]
                        ]
                    ]
                ]
            ]
        ]);
    }

    /** @test **/
    public function it_can_delete_an_order()
    {
        // Arrange
        $this->withoutExceptionHandling();
        $order = $this->createOrderWithRowsWithRealProducts();

        // Act
        $response = $this->json('DELETE', '/orders/1');
    
        // Assert
        $response->assertStatus(200);
        $this->assertDatabaseMissing('orders', [
            'id' => 1
        ]);
    }

    /** @test **/
    public function it_can_cancel_an_order()
    {
        // Arrange
        Event::fake();
        $this->withoutExceptionHandling();
        $order = $this->createOrderWithRows();
    
        // Act
        $response = $this->json('POST', 'orders/1/cancel');
    
        // Assert
        $response->assertStatus(200);
        $this->assertDatabaseMissing('orders', [
            'id' => 1,
            'cancelled_at' => ''
        ]);
        Event::assertDispatched(OrderCancelled::class, function ($e) use ($order) {
            return $e->order->id === $order->id;
        });
    }

    /** @test **/
    public function it_will_return_when_cancelling_if_not_found()
    {
        $this->withoutExceptionHandling();

        // Act
        $response = $this->json('POST', '/orders/1/cancel');
    
        // Assert
        $response->assertStatus(404);
    }

    /** @test **/
    public function it_can_process_an_order()
    {
        // Arrange
        Event::fake();
        $this->withoutExceptionHandling();
        $order = $this->createOrderWithRows();
    
        // Act
        $response = $this->json('POST', 'orders/1/process');
    
        // Assert
        $response->assertStatus(200);
        $this->assertDatabaseMissing('orders', [
            'id' => 1,
            'processed_at' => ''
        ]);
        Event::assertDispatched(OrderProcessed::class, function ($e) use ($order) {
            return $e->order->id === $order->id;
        });
    }

    /** @test **/
    public function it_will_return_404_if_no_order_can_be_found_to_process()
    {
        // Act
        $response = $this->json('POST', 'orders/1/process');
    
        // Assert
        $response->assertStatus(404);
    }

    /** @test **/
    public function it_can_create_a_new_order()
    {
        // Arrange
        Event::fake();
        $this->withoutExceptionHandling();
    
        // Act
        $response = $this->json('POST', '/orders', [
            'customer' => [
                'id' => 1
            ],
            'custom_properties' => [
                'invoice_address' => 'Vägen 19',
                'delivery_address' => 'The road 89'
            ],
            'cart' => [
                '219380XA' => [
                    'id' => 1,
                    'qty' => 2,
                    'name' => 'Prod 1',
                    'price' => 800.0,
                    'options' => [
                        "size" => 44,
                        "color" => "red",
                        "sku" => "AR2938"
                    ]
                ],
                '12938RA' => [
                    'id' => 2,
                    'qty' => 1,
                    'name' => 'Prod 2',
                    'price' => 100.0,
                    'options' => [
                        "size" => 44,
                        "color" => "red",
                        "sku" => "AR293899"
                    ]
                ]
            ]
        ]);
    
        // Assert
        $response->assertStatus(201);
        $response->assertJson([
            'data' => [
                'id' => 1,
                'custom_properties' => [
                    'invoice_address' => 'Vägen 19',
                    'delivery_address' => 'The road 89'
                ]
            ]
        ]);
        $this->assertDatabaseHas('orders', [
            'id' => 1,
            'user_id' => 1,
            'custom_properties' => "{\"invoice_address\":\"V\\u00e4gen 19\",\"delivery_address\":\"The road 89\"}"
        ]);
        $order = json_decode($response->content())->data;
        Event::assertDispatched(OrderCreated::class, function ($e) use ($order) {
            return $e->order->id === $order->id;
        });
    }

    protected function createOrderWithRows($productName = 'Orchestra')
    {
        $repo = new OrderRepository(new Order, app(\Illuminate\Contracts\Events\Dispatcher::class));
        $item = $repo->create(['rows'], [
            'customer' => [
                'id' => 193
            ],
            'cart' => [
                '219380XA' => [
                    'id' => 1,
                    'qty' => 2,
                    'name' => $productName,
                    'price' => 800.0,
                    'options' => [
                        "size" => 44,
                        "color" => "red",
                        "sku" => "AR2938"
                    ]
                ],
                '12938RA' => [
                    'id' => 2,
                    'qty' => 1,
                    'name' => 'Red dead redemption',
                    'price' => 100.0,
                    'options' => [
                        "size" => 44,
                        "color" => "red",
                        "sku" => "AR293899"
                    ]
                ]
            ]
        ]);
        return $item;
    }

    protected function createOrderWithRowsWithRealProducts()
    {
        $product = \Infab\Shop\Product::create([
            'sku' => 'AE9301',
            'name' => 'El chapo',
            'price' => 1400
        ]);
        $repo = new OrderRepository(new Order, app(\Illuminate\Contracts\Events\Dispatcher::class));
        $item = $repo->create(['rows'], [
            'customer' => [
                'id' => 193
            ],
            'cart' => [
                '219380XA' => [
                    'id' => $product->id,
                    'qty' => 1,
                    'name' => $product->name,
                    'price' => $product->price,
                    'options' => [
                        "sku" => $product->sku
                    ]
                ],
            ]
        ]);

        return $item;
    }
}
